#!/bin/bash
set -e

cond="E_50_Angle_0.0_Theta_2.1"
sampling="dpmpp_2m_logSNR_steps25"
output_file="merged_Geo_SiW_${cond}_${sampling}.png"

echo "Merging plots for $output_file"
convert +append CellLogEnergy*.png TotalEventEnergy*.png LongEventEnergy*.png -resize 3200x E_merged.png
convert RadTotalEnergy*.png AzimTotalEnergy*.png LongTotalEnergy*.png +append Prof_merged.png
convert -append E_merged.png Prof_merged.png -resize 3200x $output_file
rm E_merged.png Prof_merged.png
