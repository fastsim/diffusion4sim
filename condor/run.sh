#!/bin/bash
SCRIPT_PATH=$1
echo "Running $SCRIPT_PATH"
date; hostname; pwd

# setup the environment
nvidia-smi
source /cvmfs/sft.cern.ch/lcg/views/LCG_105_cuda/x86_64-el9-gcc11-opt/setup.sh
pip install -r requirements.txt

# run the script
python $SCRIPT_PATH
