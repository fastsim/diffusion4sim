#!/bin/bash
# set -e
export CUDA_VISIBLE_DEVICES=0

# print environment
nvidia-smi
hostname;pwd;date


declare -A model_path=(
    ["ts_cpu"]="./chkpt/conversions/cd_cpu.pt"
    ["ts_gpu"]="./chkpt/conversions/cd_gpu.pt"
    ["onnx_cpu"]="./chkpt/conversions/cd.onnx"
    ["onnx_gpu"]="./chkpt/conversions/cd.onnx"
)

echo "Running evaluation..."

for model in "${!model_path[@]}"; do
    output_dir="simulations/${model}"

    if [ -d $output_dir ]; then
        echo "Skipping..."
    else
        python scripts/validate_cpp.py \
            validate.model_path=${model_path[$model]} \
            validate.model_type=${model} \
            validate.output_dir=${output_dir}
    fi
done
echo "Done."