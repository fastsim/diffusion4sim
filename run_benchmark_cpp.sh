#!/bin/bash
# set -e
export CUDA_VISIBLE_DEVICES=0

# print environment
nvidia-smi
hostname;pwd;date


models=("ts_cpu" "ts_gpu" "onnx_cpu" "onnx_gpu")

declare -A model_path=(
    ["ts_cpu"]="./chkpt/conversions/cd_cpu.pt"
    ["ts_gpu"]="./chkpt/conversions/cd_gpu.pt"
    ["onnx_cpu"]="./chkpt/conversions/cd.onnx"
    ["onnx_gpu"]="./chkpt/conversions/cd.onnx"
)

echo "Running benchmark..."

for model in "${models[@]}"; do
    echo "Running benchmark for ${model}"
    batch_size=1
    
    python scripts/benchmark_cpp.py \
        benchmark.model_path=${model_path[$model]} \
        benchmark.model_type=${model} \
        benchmark.name=${model} \
        benchmark.output_path="benchmark_cpp.csv" \
        benchmark.batch_size=${batch_size} \
        benchmark.num_samples=10 \
        benchmark.num_rounds=5 \

done

echo "Done."