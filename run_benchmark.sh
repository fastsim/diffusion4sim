#!/bin/bash
# set -e
export CUDA_VISIBLE_DEVICES=1

# print environment
nvidia-smi
hostname;pwd;date


models=("heun_32_large cd_1_large")

declare -A model_path=(
    ["heun_32_large"]="./chkpt/experiments/calodit_edm/edm_multi_resume/2024-10-19_09-17-03/final_model.pt"
    ["cd_1_large"]="./chkpt/experiments/calodit_consistency_distillation/cd_1_large_resume/2024-11-01_03-02-57/final_model.pt"
)

declare -A diffusion_class=(
    ["heun_32_large"]="src.diffusion.EDM"
    ["cd_1_large"]="src.diffusion.ConsistencyModel"
)


declare -A sampling_args=(
    ["heun_32_large"]="sampling.solver=heun sampling.steps=32 "
    ["cd_1_large"]="sampling.steps=1 "
)

echo "Running benchmark..."

for model in "${models[@]}"; do
    echo "Running benchmark for ${model}"

    for device in single_core_cpu gpu gpu_batching_3 gpu_batching_26; do

        if [ ${device} == "single_core_cpu" ]; then
            device_args="accelerator.cpu=True accelerator.single_core=True "
            batch_size=1
        elif [ ${device} == "gpu" ]; then
            device_args="accelerator.cpu=False "
            batch_size=1
        elif [ ${device} == "gpu_batching_3" ]; then
            device_args="accelerator.cpu=False "
            batch_size=3
        elif [ ${device} == "gpu_batching_26" ]; then
            device_args="accelerator.cpu=False "
            batch_size=26
        else
            device_args="accelerator.cpu=True accelerator.single_core=True "
            batch_size=1
        fi
        
        python scripts/benchmark.py \
            model.diffusion_class=${diffusion_class[$model]} \
            model.model_path=${model_path[$model]} \
            benchmark.name=${model} \
            benchmark.output_path="benchmark.csv" \
            benchmark.batch_size=${batch_size} \
            benchmark.num_samples=10 \
            benchmark.num_rounds=5 \
            ${sampling_args[$model]} \
            ${device_args} 
    done
done

echo "Done."