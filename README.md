# Diffusion4Sim (CaloDiT)

## Description

With this repository you can train and evaluate diffusion models for fast simulation of calorimeter showers. Currently the following methods are supported:

* DDPM[^ddpm] [^iddpm] and DDIM[^ddim]
* EDM[^edm]
* DPM-Solver[^dpm] and DPM-Solver++[^dpmpp]
* Consistency Distillation[^cd]
* Easy Consistency Tuning[^ect]

It also features:

* DiT[^dit]-like transformer architecture
* multi-GPU training with [Accelerate](https://github.com/huggingface/accelerate)
* training progress tracking with [Weights & Biases](https://wandb.ai/site)
* conversion to c++ inference compatible forms (ONNX & TorchScript)
* training, distillation, validation and generation pipelines
* ...and more

To train the models, you can use one of the datasets provide in the [CaloChallenge](https://calochallenge.github.io/homepage/). Keep in mind that some changes in the `src/data/geometry.py` file might be needed to load the data properly.

**Note: Model available in [Par04](https://gitlab.cern.ch/fastsim/par04/-/tree/CaloDiT_v1) is based on EDM and distilled using consistency distillation.**

## Installation
```
# clone repository
git clone https://gitlab.cern.ch/fastsim/diffusion4sim.git
cd diffusion4sim

# create environment and install dependencies
conda create -n diffusion4sim python=3.9
conda activate diffusion4sim
pip install -r requirements.txt

# copy the examplar .env file and fill in the paths and W&B credentials
cp .env-example .env && vim .env
```

## Usage

### Training

Modify the appropriate configuration file in the `configs/train` directory to specify all hyperparameters. Refer `configs/train/edm_allegro_scratch.yaml` Then, run the training script with the path to the config file as the first argument. You can also specify command line arguments to override the config values. For example:

```
python scripts/train.py configs/train/edm.yaml experiment.run_name="edm_calo_challenge_dataset2"
```

The training script will save the model checkpoints, logs and intermediate evaluation results in the specified `output_dir` directory. You can track the training progress with Weights & Biases by setting the `use_wandb` parameter in the config file.

To run multi-gpu training, use the following command:

```
accelerate launch --multi_gpu --num_processes=<num_gpus> scripts/train.py configs/train/edm_allegro_scratch.yaml
```

For more information, see the [Accelerate documentation](https://huggingface.co/docs/accelerate/index).

### Distillation

Similarly to training, you run distillation using the `scripts/distill.py` script. The distillation process requires the teacher model checkpoint, which should be specified in the config file as `model_path`. Currently only the Consistency Distillation[^cd] is supported. Refer `configs/distill/cd_allegro_scratch.yaml`.

### Conversion

Conversion to ONNX or TorchScript models can be done using `scripts/convert.py`. Refer the file as an example.

### Validation

Use the `scripts/validate.py` to generate showers under the same conditions as in the provided file, and compare with this reference data. The script will save generated showers and create the plots comparing observables of generated samples against reference showers. See the `run_evaluation.sh` script for an example.

### Generation

Use the `scripts/generate.py` to generate showers from the trained model under the specified conditions. The generated showers will be saved in the `output_dir` directory in the .h5 format for further analysis.

## Multi-geometry training and adaptation

### Training

If you need to pretrain on multiple geometries, follow the instructions below. A couple of changes to inform about the geometry is needed, mostly in the .yaml files. See `configs/train/edm_multi.yaml` for an example.
* Change the way data files are described both during training and testing. Use `[geometry_name, file_path]` instead of `file_path`.
* Setup `need_geo_condn` & `train_on`. In model architecture, change `conditions_size`. This should be one more than the length of `train_on` if you are interested in finetuning over a new detector later.
* *Recommended to use the standarization based on all of your pretraining geometries.*

**Distillation:** Make the above changes for the distillation .yaml file and mention the teacher checkpoint of the pretrained model as stated previously. Refer `configs/distill/cd_multi.yaml`

### Adaptation

You would need a pretrained model to finetune. Get one from [this link](https://cernbox.cern.ch/remote.php/dav/public-files/KpHt35z1CRZ3WGN/edm_multi_final_model.pt) or following the previous steps. Adaptation is done by re-training the pretrained model using a low learning rate. So .yaml file will be same as the one used in multi-geometry training, except:
* Don't use a very high learning rate. Anything less than or equal to 1e-3 should be fine.
* Fewer training steps, i.e., `max_steps` and respective lr_schedular args.
* Data files. Keep `train_on` the same as before. Any detector geometry not in the list will be the last one, i.e., `[0 0 ... 1]`.
* Mention the checkpoint on the pretrained model as `model_path` under `model` params.
* Refer to `configs/train/edm_allegro_adapt.yaml`

**Distillation:**
* If you are not doing the pretraining yourselves, get the pretrained distilled model from [this link](https://cernbox.cern.ch/remote.php/dav/public-files/kFWKg1M3RfYMep6/cd_multi_final_model.pt).
* Mention checkpoints in .yaml file for the distillation. Teacher checkpoint is pretrained EDM model. Student checkpoint is pretrained distilled model.
* Make sure `init_student_from_teacher` is `false`.
* Refer `configs/distill/cd_allegro_adapt.yaml` for more details.

## Credits
This repository is derived from the [Diffusion4FastSim](https://gitlab.cern.ch/mpiorczy/diffusion4fastsim), however it also benefits a lot from the code implemented in the following repositories:
* [denoising-diffusion-pytorch](https://github.com/lucidrains/denoising-diffusion-pytorch)
* [improved-diffusion](https://github.com/openai/improved-diffusion)
* [k-diffusion](https://github.com/crowsonkb/k-diffusion)
* [consistency_models](https://github.com/openai/consistency_models)
* [dpm-solver](https://github.com/LuChengTHU/dpm-solver)
* [DiT](https://github.com/facebookresearch/DiT)

---
[^ddpm]: Ho, Jonathan, Ajay Jain, and Pieter Abbeel. ["Denoising diffusion probabilistic models."](https://arxiv.org/abs/2006.11239) Advances in neural information processing systems 33 (2020): 6840-6851.  
[^iddpm]: Nichol, Alexander Quinn, and Prafulla Dhariwal. ["Improved denoising diffusion probabilistic models."](https://arxiv.org/abs/2102.09672) International conference on machine learning. PMLR, 2021.  
[^ddim]: Song, Jiaming, Chenlin Meng, and Stefano Ermon. ["Denoising diffusion implicit models."](https://arxiv.org/abs/2010.02502) arXiv preprint arXiv:2010.02502 (2020).  
[^edm]: Karras, Tero, et al. ["Elucidating the design space of diffusion-based generative models."](https://arxiv.org/abs/2206.00364) Advances in neural information processing systems 35 (2022): 26565-26577.
[^dpm]: Lu, Cheng, et al. "Dpm-solver: A fast ode solver for diffusion probabilistic model sampling in around 10 steps." Advances in Neural Information Processing Systems 35 (2022): 5775-5787.  
[^dpmpp]: Lu, Cheng, et al. ["Dpm-solver++: Fast solver for guided sampling of diffusion probabilistic models."](https://arxiv.org/abs/2211.01095) arXiv preprint arXiv:2211.01095 (2022).  
[^cd]: Song, Yang, et al. ["Consistency models."](https://arxiv.org/abs/2303.01469) arXiv preprint arXiv:2303.01469 (2023).  
[^ect]: Geng, Zhengyang, et al. ["Consistency Models Made Easy."](https://arxiv.org/abs/2406.14548) arXiv preprint arXiv:2406.14548 (2024).  
[^dit]: Peebles, William, and Saining Xie. ["Scalable diffusion models with transformers."](https://arxiv.org/abs/2212.09748) Proceedings of the IEEE/CVF International Conference on Computer Vision. 2023.
