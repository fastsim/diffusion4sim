import math

import torch
import torch.nn as nn


def cosine_beta_schedule(timesteps, s=0.008):
    """Proposed in https://arxiv.org/abs/2102.09672"""
    steps = timesteps + 1
    t = torch.linspace(0, timesteps, steps, dtype=torch.float64) / timesteps
    alphas_bar = torch.cos((t + s) / (1 + s) * math.pi / 2) ** 2
    alphas_bar = alphas_bar / alphas_bar[0]
    betas = 1 - (alphas_bar[1:] / alphas_bar[:-1])
    return torch.clip(betas, 0.0001, 0.9999)


def linear_beta_schedule(timesteps):
    """Proposed originally in DDPM paper"""
    beta_start = 1e-4
    beta_end = 0.02
    return torch.linspace(beta_start, beta_end, timesteps, dtype=torch.float64)


def sigmoid_beta_schedule(timesteps, start=-3, end=3, tau=1.0):
    """Proposed in https://arxiv.org/abs/2212.11972, works better for large images, when used during training"""
    steps = timesteps + 1
    t = torch.linspace(0, timesteps, steps, dtype=torch.float64) / timesteps
    v_start = torch.sigmoid(torch.tensor(start / tau))
    v_end = torch.sigmoid(torch.tensor(end / tau))
    alphas_bar = (-torch.sigmoid((t * (end - start) + start) / tau) + v_end) / (v_end - v_start)
    alphas_bar = alphas_bar / alphas_bar[0]
    betas = 1 - (alphas_bar[1:] / alphas_bar[:-1])
    return torch.clip(betas, 0, 0.999)


class BetaSchedule(nn.Module):
    def __init__(self, beta_schedule="cosine", beta_schedule_kwargs=dict()):
        super().__init__()
        self.beta_schedule = beta_schedule
        if beta_schedule == "cosine":
            self.beta_schedule_fn = cosine_beta_schedule
        elif beta_schedule == "linear":
            self.beta_schedule_fn = linear_beta_schedule
        elif beta_schedule == "sigmoid":
            self.beta_schedule_fn = sigmoid_beta_schedule
        else:
            raise NotImplementedError(f"Unknown beta schedule {beta_schedule}")

        self.beta_schedule_kwargs = beta_schedule_kwargs

    def forward(self, timesteps):
        return self.beta_schedule_fn(timesteps, **self.beta_schedule_kwargs)
    

    def extra_repr(self):
        return f"beta_schedule={self.beta_schedule}"
