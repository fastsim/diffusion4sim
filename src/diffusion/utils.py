from typing import List, Union

import torch


def extract_into_tensor(arr, timesteps, broadcast_shape):
    """
    Extracts values from the input array based on the specified timesteps and reshapes it to match the broadcast shape.

    Args:
        arr (torch.Tensor): The input array.
        timesteps (torch.Tensor): The timesteps to extract from the input array.
        broadcast_shape (tuple): The desired shape of the output tensor.

    Returns:
        torch.Tensor: The extracted values reshaped to match the broadcast shape.
    """
    res = arr.to(device=timesteps.device)[timesteps].float()
    res = res.reshape(-1, *((1,) * (len(broadcast_shape) - 1)))
    return res.expand(broadcast_shape)


def respace_timesteps(num_timesteps: int, sampling_steps: Union[int, List[int]], ddim_striding=False):
    """
    Create a list of timesteps to use from an original diffusion process.
    
    Args:
        num_timesteps (int): The original number of diffusion steps.
        sampling_steps (Union[int, List[int]]): The number of steps to sample at each section.
            For example, if there's 300 timesteps and the section counts are [10,15,20]
            then the first 100 timesteps are strided to be 10 timesteps, the second 100
            are strided to be 15 timesteps, and the final 100 are strided to be 20.
        ddim_striding (bool, optional): Wheter to use fixed striding from the DDIM paper. Then only one section is allowed.

    Returns:
        torch.Tensor: A tensor of diffusion steps from the original process to use.
    """

    if ddim_striding:
        # See Section 4 of https://arxiv.org/pdf/2102.09672
        assert isinstance(sampling_steps, int), "ddim_striding only supports one section"
        for i in range(1, num_timesteps):
            if len(range(0, num_timesteps, i)) == sampling_steps:
                return torch.arange(0, num_timesteps, i)
        raise ValueError(f"Cannot create exactly {sampling_steps} steps with an integer stride")
        
    if isinstance(sampling_steps, int):
        sampling_steps = [sampling_steps]

    size_per = num_timesteps // len(sampling_steps)
    extra = num_timesteps % len(sampling_steps)
    start_idx = 0
    all_steps = []
    for i, sampling_steps in enumerate(sampling_steps):
        size = size_per + (1 if i < extra else 0)
        if size < sampling_steps:
            raise ValueError(f"Cannot divide section of {size} steps into {sampling_steps}")
        if sampling_steps <= 1:
            frac_stride = 1
        else:
            frac_stride = (size - 1) / (sampling_steps - 1)
        cur_idx = 0.0
        taken_steps = []
        for _ in range(sampling_steps):
            taken_steps.append(start_idx + round(cur_idx))
            cur_idx += frac_stride
        all_steps += taken_steps
        start_idx += size
    return torch.tensor(all_steps)