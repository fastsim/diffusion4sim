"""
Consistency Distillation (CD) from: https://arxiv.org/abs/2303.01469. 
Based on the: https://github.com/openai/consistency_models
"""

import copy

import torch

from src.diffusion.edm import EDM
from src.diffusion.sampling import sample_multistep, sample_singlestep, to_d
from src.diffusion.utils import respace_timesteps
from src.distiller import DiffusionDistillationTrainer
from src.models.ema import ema_update
from src.utils import append_dims, mean_flat, unwrap_ddp


class ConsistencyModel(EDM):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def forward(self, x, x_cond):
        raise NotImplementedError("Currently only consistency distillation is supported.")

    def get_scalings(self, sigma):
        c_skip = self.sigma_data**2 / ((sigma - self.sigma_min) ** 2 + self.sigma_data**2)
        c_out = (sigma - self.sigma_min) * self.sigma_data / (sigma**2 + self.sigma_data**2) ** 0.5
        c_in = 1 / (sigma**2 + self.sigma_data**2) ** 0.5
        return c_skip, c_out, c_in

    @torch.inference_mode()
    def sample(
        self, conditions, steps=1, discretization_steps=40, progress=False, clip_denoised=False
    ):
        num_samples = len(conditions[0])
        assert all(num_samples == len(c) for c in conditions), "all conditions must have the same batch size"
        x_shape = (num_samples, *self.input_size)

        ts = self.get_timesteps(discretization_steps)
        sigmas = self.get_sigmas(ts)

        solver_args = {}
        if steps > 1:
            time_steps = respace_timesteps(discretization_steps, steps+1)
            sigmas = sigmas[time_steps]
            solver_args = {"sigma_min": self.sigma_min, "sigma_max": self.sigma_max}

        x_T = torch.randn(x_shape, device=self.device) * self.sigma_max

        sample_fn = sample_singlestep if steps == 1 else sample_multistep

        def denoiser(x_t, sigma):
            denoised = self.denoise(x_t, conditions, sigma)
            if clip_denoised:
                denoised = denoised.clamp(-1, 1)
            return denoised

        x_0 = sample_fn(
            denoiser,
            x_T,
            sigmas,
            progress=progress,
            **solver_args
        )
        return x_0


class ConsistencyDistillationTrainer(DiffusionDistillationTrainer):
    def __init__(
        self,
        teacher_model: EDM,
        student_model: ConsistencyModel,
        *args,
        target_ema_decay=0.95,
        discretization_steps=18,
        metric_function="l2",
        solver="heun",
        **kwargs,
    ):
        super().__init__(teacher_model, student_model, *args, **kwargs)

        self.discretization_steps = discretization_steps  # N in Algorithm 2
        self.metric_function = metric_function  # d(.,.) in Algorithm 2
        self.solver = solver  # \Phi in Algorithm 2

        target_model = copy.deepcopy(student_model)
        for dst, src in zip(target_model.parameters(), student_model.parameters()):
            dst.data.copy_(src.data)

        self.target_model = self.accelerator.prepare(target_model)
        self.target_model.requires_grad_(False)
        self.target_model.train()

        self.target_ema_decay = target_ema_decay

    def update_target_model(self):
        ema_update(self.model, self.target_model, self.target_ema_decay)

    def loss_fn(self, student_model, x_0, x_cond):
        # equation 7. from https://arxiv.org/abs/2303.01469

        def student_denoise_fn(x, t):
            return unwrap_ddp(student_model).denoise(x, x_cond, t)
            
        @torch.no_grad()
        def target_denoise_fn(x, t):
            return unwrap_ddp(self.target_model).denoise(x, x_cond, t)

        @torch.no_grad()
        def teacher_denoise_fn(x, t):
            return unwrap_ddp(self.teacher_model).denoise(x, x_cond, t)

        @torch.no_grad()
        def euler_solver(x, t, t_next):
            dt = append_dims(t_next - t, x.ndim)
            denoised = teacher_denoise_fn(x, t)
            d = to_d(x, t, denoised)
            x = x + d * dt

            return x

        @torch.no_grad()
        def heun_solver(x, t, t_next):
            dt = append_dims(t_next - t, x.ndim)
            denoised = teacher_denoise_fn(x, t)
            d = to_d(x, t, denoised)
            x_next = x + d * dt
            denoised_next = teacher_denoise_fn(x_next, t_next)
            d_next = to_d(x_next, t_next, denoised_next)
            d_prime = (d + d_next) / 2
            x = x + d_prime * dt

            return x

        ts = unwrap_ddp(student_model).get_timesteps(self.discretization_steps)
        indices = torch.randint(0, self.discretization_steps - 1, (x_0.shape[0],), device=x_0.device)

        t = ts[indices]
        t_next = ts[indices + 1]

        eps = torch.randn_like(x_0)
        x_t = x_0 + eps * append_dims(t, x_0.ndim)

        dropout_state = torch.get_rng_state()
        x_0_hat = student_denoise_fn(x_t, t)

        solver_fn = {
            "euler": euler_solver,
            "heun": heun_solver,
        }[self.solver]

        x_t_next = solver_fn(x_t, t, t_next).detach()

        torch.set_rng_state(dropout_state)
        x_0_hat_next = target_denoise_fn(x_t_next, t_next).detach()

        if self.metric_function == "l1":
            losses = mean_flat(torch.abs(x_0_hat - x_0_hat_next))
        elif self.metric_function == "l2":
            losses = mean_flat((x_0_hat - x_0_hat_next) ** 2)
        else:
            raise ValueError(f"Unknown loss norm {self.loss_norm}")

        return losses.mean()

    def _training_step(self):
        running_loss = super()._training_step()

        # update target model, equation 8. from https://arxiv.org/abs/2303.01469
        self.update_target_model()

        return running_loss
