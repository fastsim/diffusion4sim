"""
Denoising Diffusion Implicit Models (DDIM) from: https://arxiv.org/pdf/2010.02502.
Based on the: https://github.com/openai/improved-diffusion
"""

import torch

from src.diffusion.ddpm import DDPM
from src.diffusion.utils import extract_into_tensor


class DDIM(DDPM):
    def __init__(self, eta=0.0, **kwargs):
        super().__init__(ddim_striding=True, **kwargs)
        self.eta = eta

    @torch.inference_mode()
    def sample_step(self, x_t, x_cond, t):
        """Sample x_{t-1} from the model using DDIM."""
        eps, x_start = self.model_predictions(x_t, x_cond, t)
        alpha_bar = extract_into_tensor(self.alphas_cumprod, t, x_t.shape)
        alpha_bar_prev = extract_into_tensor(self.alphas_cumprod_prev, t, x_t.shape)
        sigma = (
            self.eta * torch.sqrt((1 - alpha_bar_prev) / (1 - alpha_bar)) * torch.sqrt(1 - alpha_bar / alpha_bar_prev)
        )
        # equation 12. from https://arxiv.org/pdf/2010.02502
        noise = torch.randn_like(x_t)
        mean_pred = x_start * torch.sqrt(alpha_bar_prev) + torch.sqrt(1 - alpha_bar_prev - sigma**2) * eps
        nonzero_mask = (t != 0).float().view(-1, *([1] * (len(x_t.shape) - 1)))  # no noise when t == 0
        x_prev = mean_pred + nonzero_mask * sigma * noise
        return x_prev, x_start

    def reverse_sample_step(self, x_t, x_cond, t):
        """Sample x_{t+1} from the model using DDIM reverse ODE."""
        assert self.eta == 0.0, "Reverse ODE only for deterministic path"
        out = self.p_mean_variance(x_t, x_cond, t)
        # usually model outputs epsilon, but we re-derive it in case x_0 or v prediction
        eps = (
            extract_into_tensor(self.sqrt_recip_alphas_cumprod, t, x_t.shape) * x_t - out["pred_xstart"]
        ) / extract_into_tensor(self.sqrt_recipm1_alphas_cumprod, t, x_t.shape)
        alpha_bar_next = extract_into_tensor(self.alphas_cumprod_next, t, x_t.shape)

        # equation 12. from https://arxiv.org/pdf/2010.02502 reversed
        mean_pred = out["pred_xstart"] * torch.sqrt(alpha_bar_next) + torch.sqrt(1 - alpha_bar_next) * eps
        return mean_pred, out["pred_xstart"]
