"""
Denoising Diffusion Probabilistic Models (DDPM) from: https://arxiv.org/abs/2006.11239
Based on the: https://github.com/lucidrains/denoising-diffusion-pytorch
"""

import logging
from functools import partial
from typing import Iterable

import torch
import torch.nn as nn
import torch.nn.functional as F
from tqdm import tqdm

from src.diffusion.base import DiffusionModelBase
from src.diffusion.schedule import BetaSchedule
from src.diffusion.utils import extract_into_tensor, respace_timesteps
from src.utils import default, get_logger, identity

logger = get_logger()

class DDPMLoss(nn.Module):
    def __init__(self, loss_type="l2"):
        super().__init__()
        self.loss_type = loss_type
        if loss_type == "l1":
            self.loss_fn = F.l1_loss
        elif loss_type == "l2":
            self.loss_fn = F.mse_loss
        elif loss_type == "huber":
            self.loss_fn = F.huber_loss
        else:
            raise NotImplementedError(f"Unknown loss type {loss_type}")

    def forward(self, y_true, y_pred):
        return self.loss_fn(y_true, y_pred)

    def extra_repr(self):
        return f"loss_type={self.loss_type}"


class DDPM(DiffusionModelBase):
    def __init__(
        self,
        model,
        num_timesteps=400,
        sampling_steps=None,
        ddim_striding=False,
        beta_schedule="cosine",
        beta_schedule_args=dict(),
        loss_type="l2",
        objective_type="pred_noise",
    ):
        super().__init__()
        self.model = model
        assert model.in_channels == model.out_channels, "input and output channels must match"
        self.input_size = (model.in_channels, *model.input_size)

        self.num_timesteps = num_timesteps
        timesteps = torch.arange(num_timesteps)
        self.register_buffer('timesteps', timesteps)

        self.sampling_steps = default(sampling_steps, num_timesteps)

        maybe_sum = lambda x: sum(x) if isinstance(x, Iterable) else x
        assert maybe_sum(self.sampling_steps) <= self.num_timesteps
        self.is_strided_sampling = maybe_sum(self.sampling_steps) < self.num_timesteps

        # loss function
        self.loss_type = loss_type
        self.loss_fn = DDPMLoss(loss_type)

        # model parametrization
        self.objective_type = objective_type
        
        # betas
        self.beta_schedule = BetaSchedule(beta_schedule, beta_schedule_args)
        betas = self.beta_schedule(num_timesteps)
        assert len(betas.shape) == 1, "betas must be 1-D"
        assert (betas > 0).all() and (betas <= 1).all(), "betas must be in (0, 1]"
        self.register_buffer('betas', betas)

        # alphas
        alphas = 1.0 - self.betas
        alphas_cumprod = alphas.cumprod(dim=0)
        self.register_buffer('alphas', alphas)
        self.register_buffer('alphas_cumprod', alphas_cumprod)

        if self.is_strided_sampling:
            logger.info(f"Respacing {self.num_timesteps} timesteps to {self.sampling_steps} steps")
            self.org_num_timesteps = num_timesteps
            timesteps = respace_timesteps(num_timesteps, sampling_steps, ddim_striding=ddim_striding)
            self.register_buffer('timesteps', timesteps)
            self.num_timesteps = len(timesteps)

            betas = 1 - self.alphas_cumprod[self.timesteps] / F.pad(
                self.alphas_cumprod[self.timesteps][:-1], (1, 0), value=1.0
            )
            alphas = 1.0 - betas
            alphas_cumprod = alphas.cumprod(dim=0)
            self.register_buffer('betas', betas)
            self.register_buffer('alphas', alphas)
            self.register_buffer('alphas_cumprod', alphas_cumprod)

        alphas_cumprod_prev = F.pad(self.alphas_cumprod[:-1], (1, 0), value=1.0)
        alphas_cumprod_next = F.pad(self.alphas_cumprod[1:], (0, 1), value=0.0)
        self.register_buffer('alphas_cumprod_prev', alphas_cumprod_prev)
        self.register_buffer('alphas_cumprod_next', alphas_cumprod_next)

        # calculations for diffusion q(x_t | x_{t-1}) and others
        sqrt_alphas_cumprod = torch.sqrt(self.alphas_cumprod)
        sqrt_one_minus_alphas_cumprod = torch.sqrt(1.0 - self.alphas_cumprod)
        sqrt_recip_alphas_cumprod = torch.sqrt(1.0 / self.alphas_cumprod)
        sqrt_recipm1_alphas_cumprod = torch.sqrt(1.0 / self.alphas_cumprod - 1)
        self.register_buffer('sqrt_alphas_cumprod', sqrt_alphas_cumprod)
        self.register_buffer('sqrt_one_minus_alphas_cumprod', sqrt_one_minus_alphas_cumprod)
        self.register_buffer('sqrt_recip_alphas_cumprod', sqrt_recip_alphas_cumprod)
        self.register_buffer('sqrt_recipm1_alphas_cumprod', sqrt_recipm1_alphas_cumprod)

        # calculations for posterior q(x_{t-1} | x_t, x_0)
        sqrt_recip_alphas = torch.sqrt(1.0 / self.alphas)
        posterior_variance = self.betas * (1.0 - self.alphas_cumprod_prev) / (1.0 - self.alphas_cumprod)
        posterior_log_variance_clipped = torch.log(
            F.pad(posterior_variance[1:], (1, 0), value=posterior_variance[1])
        )
        posterior_mean_x0 = self.betas * torch.sqrt(self.alphas_cumprod_prev) / (1.0 - self.alphas_cumprod)
        posterior_mean_xt = (
            (1.0 - self.alphas_cumprod_prev) * torch.sqrt(self.alphas) / (1.0 - self.alphas_cumprod)
        )
        self.register_buffer('sqrt_recip_alphas', sqrt_recip_alphas)
        self.register_buffer('posterior_variance', posterior_variance)
        self.register_buffer('posterior_log_variance_clipped', posterior_log_variance_clipped)
        self.register_buffer('posterior_mean_x0', posterior_mean_x0)
        self.register_buffer('posterior_mean_xt', posterior_mean_xt)

    def sample_timesteps(self, x_like, skip_first=False):
        t_min = 1 if skip_first else 0
        t_max = self.num_timesteps
        return torch.randint(t_min, t_max, size=(x_like.shape[0],), device=x_like.device, dtype=torch.long)

    def q_sample(self, x_0, t, eps=None):
        """Sample from q(x_t | x_0) using the `nice property` aka forward diffusion process."""
        eps = default(eps, torch.randn_like(x_0))
        return (
            extract_into_tensor(self.sqrt_alphas_cumprod, t, x_0.shape) * x_0
            + extract_into_tensor(self.sqrt_one_minus_alphas_cumprod, t, x_0.shape) * eps
        )

    def q_posterior_mean_variance(self, x_0, x_t, t):
        """Compute the mean and variance of the posterior q(x_{t-1} | x_t, x_0)."""
        posterior_mean = (
            extract_into_tensor(self.posterior_mean_x0, t, x_t.shape) * x_0
            + extract_into_tensor(self.posterior_mean_xt, t, x_t.shape) * x_t
        )
        posterior_variance = extract_into_tensor(self.posterior_variance, t, x_t.shape)
        posterior_log_variance_clipped = extract_into_tensor(self.posterior_log_variance_clipped, t, x_t.shape)
        return posterior_mean, posterior_variance, posterior_log_variance_clipped

    def _predict_xstart_from_eps(self, x_t, t, eps):
        return (
            extract_into_tensor(self.sqrt_recip_alphas_cumprod, t, x_t.shape) * x_t
            - extract_into_tensor(self.sqrt_recipm1_alphas_cumprod, t, x_t.shape) * eps
        )

    def _predict_eps_from_xstart(self, x_t, t, x_0):
        return (extract_into_tensor(self.sqrt_recip_alphas_cumprod, t, x_t.shape) * x_t - x_0) \
            / extract_into_tensor(self.sqrt_recipm1_alphas_cumprod, t, x_t.shape
        )

    def _predict_v_from_xstart(self, x_0, t, eps):
        return (
            extract_into_tensor(self.sqrt_alphas_cumprod, t, x_0.shape) * eps
            - extract_into_tensor(self.sqrt_one_minus_alphas_cumprod, t, x_0.shape) * x_0
        )

    def _predict_xstart_from_v(self, x_t, t, v):
        return (
            extract_into_tensor(self.sqrt_alphas_cumprod, t, x_t.shape) * x_t
            - extract_into_tensor(self.sqrt_one_minus_alphas_cumprod, t, x_t.shape) * v
        )

    def model_predictions(self, x_t, x_cond, t, clip_denoised=False, rederive_pred_noise=False):
        rescaled_t = self.timesteps[t]  # respace timesteps in case of strided sampling
        model_output = self.model(x_t, x_cond, rescaled_t)
        maybe_clip = partial(torch.clamp, min=-1.0, max=1.0) if clip_denoised else identity

        if self.objective_type == "pred_noise":
            pred_noise = model_output
            pred_xstart = self._predict_xstart_from_eps(x_t, t, eps=pred_noise)
            pred_xstart = maybe_clip(pred_xstart)

            if clip_denoised and rederive_pred_noise:
                pred_noise = self._predict_eps_from_xstart(x_t, t, x_0=pred_xstart)

        elif self.objective_type == "pred_xstart":
            pred_xstart = model_output
            pred_xstart = maybe_clip(pred_xstart)
            pred_noise = self._predict_eps_from_xstart(x_t, t, x_0=pred_xstart)

        elif self.objective_type == "pred_v":
            v = model_output
            pred_xstart = self._predict_xstart_from_v(x_t, t, v)
            pred_xstart = maybe_clip(pred_xstart)
            pred_noise = self._predict_eps_from_xstart(x_t, t, x_0=pred_xstart)

        else:
            raise NotImplementedError(f"Unknown objective type {self.objective_type}")

        return pred_noise, pred_xstart

    def p_mean_variance(self, x_t, x_cond, t):
        """Apply the model to get p(x_{t-1} | x_t), as well as a prediction of the initial x_0."""
        pred_noise, pred_xstart = self.model_predictions(x_t, x_cond, t)
        model_mean, model_variance, model_log_variance = self.q_posterior_mean_variance(x_0=pred_xstart, x_t=x_t, t=t)
        return {
            "mean": model_mean,
            "variance": model_variance,
            "log_variance": model_log_variance,
            "pred_xstart": pred_xstart,
        }

    @torch.inference_mode()
    def sample_step(self, x_t, x_cond, t):
        """Sample x_{t-1} from the model at the given timestep."""
        out = self.p_mean_variance(x_t, x_cond, t)
        noise = torch.randn_like(x_t)
        nonzero_mask = (t != 0).float().view(-1, *([1] * (len(x_t.shape) - 1)))  # no noise when t == 0
        x_prev = out["mean"] + nonzero_mask * torch.exp(0.5 * out["log_variance"]) * noise
        return x_prev, out["pred_xstart"]

    # the above is equivalent but allows for more flexibility in the future
    # @torch.inference_mode()
    # def sample_step(self, x_t, x_cond, t):
    #     """Sample x_{t-1} from the model at the given timestep."""
    #     eps_hat = self.model(x_t, x_cond, self.timesteps[t])  # respace timesteps in case of strided sampling
    #     x_0 = self._predict_xstart_from_eps(x_t, t, eps_hat)
    #     model_mean = (
    #         extract_into_tensor(self.sqrt_recip_alphas, t, x_t.shape) * (x_t - extract_into_tensor(self.betas, t, x_t.shape) * eps_hat \
    #         / extract_into_tensor(self.sqrt_one_minus_alphas_cumprod, t, x_t.shape))
    #     )
    #     z = torch.randn_like(x_t) if all(t > 0) else 0.0  # no noise if t == 0
    #     model_variance = extract_into_tensor(self.posterior_variance, t, x_t.shape)
    #     return model_mean + torch.sqrt(model_variance) * z, x_0

    @torch.inference_mode()
    def sample_loop(self, x_shape, x_cond, progress=False):
        batch_size = x_shape[0]
        x = torch.randn(x_shape, device=self.device)

        indices = list(range(self.num_timesteps))[::-1]
        if progress:
            indices = tqdm(indices)

        for t in indices:
            ts = torch.full((batch_size,), t, device=self.device, dtype=torch.long)
            x, x_0 = self.sample_step(x, x_cond, ts)

        return x

    @torch.inference_mode()
    def sample(self, conditions, progress=False):
        num_samples = len(conditions[0])
        assert all(num_samples == len(c) for c in conditions), "all conditions must have the same batch size"
        input_shape = (num_samples, *self.input_size)
        return self.sample_loop(input_shape, conditions, progress=progress)

    def forward(self, x, x_cond):
        t = self.sample_timesteps(x)
        eps = torch.randn_like(x)
        x_t = self.q_sample(x, t, eps)
        rescaled_t = self.timesteps[t]  # respace timesteps in case of strided sampling
        preds = self.model(x_t, x_cond, rescaled_t)

        if self.objective_type == "pred_noise":
            target = eps
        elif self.objective_type == "pred_xstart":
            target = x
        elif self.objective_type == "pred_v":
            v = self._predict_v_from_xstart(x, t, eps)
            target = v
        else:
            raise NotImplementedError(f"Unknown objective type {self.objective_type}")

        loss = self.loss_fn(target, preds)
        return loss
