"""
Implementation of Easy Consistency Tuning (ECT) from https://arxiv.org/pdf/2406.14548. 
Based on the: https://github.com/locuslab/ect
"""

import math

import torch

from src.diffusion.cd import ConsistencyModel
from src.utils import append_dims, sum_flat


class ECT(ConsistencyModel):
    def __init__(self, d, q=2, k=8.0, b=1.0, c=0.0, adjust_fn="sigmoid", weighting_fn="karras", **kwargs):
        kwargs["P_mean"] = kwargs.get("P_mean", -1.1)
        kwargs["P_std"] = kwargs.get("P_std", 2.0)
        kwargs["sigma_data"] = kwargs.get("sigma_data", 0.5)
        super().__init__(**kwargs)

        self.q = q
        self.d = d  # total_iters // 8  # reduce dt every d steps
        self.adjust_fn = adjust_fn
        self.k = k
        self.b = b

        self.weighting_fn = weighting_fn
        self.c = c

    def t_to_r(self, t, iters):
        decay = 1 / self.q ** math.ceil(iters / self.d)
        if self.adjust_fn == "sigmoid":
            n_t = 1 + self.k * torch.sigmoid(-self.b * t)
        elif self.adjust_fn == "constant":
            n_t = torch.ones_like(t)
        else:
            raise ValueError(f"Unknown training schedule {self.training_schedule}")
        ratio = 1 - decay * n_t
        r = t * ratio
        return torch.clamp(r, min=0)

    def loss_weighting(self, t, r=None):
        if self.weighting_fn == "snr":
            return 1 / t**2
        elif self.weighting_fn == "snr+1":
            return 1 / t**2 + 1
        elif self.weighting_fn == "karras":
            return 1 / t**2 + 1 / self.sigma_data**2
        elif self.weighting_fn == "fd":
            return 1 / (t - r)
        elif self.weighting_fn == "uniform":
            return torch.ones_like(t)
        else:
            raise NotImplementedError()

    def forward(self, x_0, x_cond, iters):
        batch_size = x_0.shape[0]

        def denoise_fn(x, t):
            return self.denoise(x, x_cond, t)

        # t ~ p(t) and r ~ p(r|t, iters)
        t = self.noise_distribution(batch_size)
        r = self.t_to_r(t, iters)

        # shared noise direction
        eps = torch.randn_like(x_0)
        x_t = x_0 + eps * append_dims(t, x_0.ndim)
        x_r = x_0 + eps * append_dims(r, x_0.ndim)

        # shared dropout mask
        rng_state = torch.cuda.get_rng_state()
        x_0_t = denoise_fn(x_t, t)

        if r.max() > 0:
            torch.cuda.set_rng_state(rng_state)
            with torch.no_grad():
                x_0_r = denoise_fn(x_r, r)

            mask = append_dims(r > 0, x_0.ndim)
            x_0_r = torch.nan_to_num(x_0_r)
            x_0_r = mask * x_0_r + (~mask) * x_0
        else:
            x_0_r = x_0

        # L2 loss
        loss = sum_flat((x_0_t - x_0_r) ** 2)

        # adaptive weighting (p=0.5) through Huber Loss
        if self.c > 0:
            loss = torch.sqrt(loss + self.c**2) - self.c
        else:
            loss = torch.sqrt(loss)

        # loss weighting
        weights = self.loss_weighting(t, r)
        return (weights * loss).mean()
