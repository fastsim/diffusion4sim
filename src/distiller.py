from pathlib import Path

from accelerate import Accelerator
from torch.utils.data import Dataset

from src.data.preprocessing import CaloShowerPreprocessor
from src.diffusion import DiffusionModelBase
from src.trainer import DiffusionTrainer
from src.utils import get_logger

logger = get_logger()


class DiffusionDistillationTrainer(DiffusionTrainer):
    def __init__(
        self,
        teacher_model: DiffusionModelBase,
        student_model: DiffusionModelBase,
        output_dir: Path,
        train_dataset: Dataset,
        valid_dataset: Dataset,
        preprocessor: CaloShowerPreprocessor,
        accelerator: Accelerator,
        init_student_from_teacher: bool = True,
        **kwargs,
    ):
        if init_student_from_teacher:
            for dst, src in zip(student_model.parameters(), teacher_model.parameters()):
                dst.data.copy_(src.data)

        super().__init__(student_model, output_dir, train_dataset, valid_dataset, preprocessor, accelerator, **kwargs)

        self.teacher_model = self.accelerator.prepare(teacher_model)
        self.teacher_model.requires_grad_(False)
        self.teacher_model.eval()

    def loss_fn(self, student_model, x_0, x_cond):
        raise NotImplementedError("Loss computation must be implemented by subclass.")
