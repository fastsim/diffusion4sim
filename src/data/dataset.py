import logging
from pathlib import Path

import h5py
import numpy as np
import torch

from src.data.utils import load_showers
from src.utils import get_logger

logger = get_logger()


def preprocess_geo(batchsize, geo, train_on):
    len_geo = len(train_on)
    condn = np.zeros((batchsize, len_geo + 1)).astype(np.float32)
    try:
        condn[:, train_on.index(geo)] = 1
    except ValueError:
        condn[:, -1] = 1  # adaptation
    return condn


class CaloShowerDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        root_path=None,
        files=None,
        extension=".h5",
        use_cond_info=True,
        max_num_showers=None,
        need_geo_condn=False,
        train_on=None
    ):
        if root_path is None and files is None:
            raise ValueError("Either root_path or files must be passed.")
        if root_path is not None and files is not None:
            raise ValueError("Cannot pass both root_path and files.")
        
        self.root_path = Path(root_path) if root_path is not None else None
        self.files = list(self.root_path.glob(f"*{extension}")) if files is None else files

        self.need_geo_condn = need_geo_condn
        if isinstance(files[0], str):
            # we have geometry info
            assert self.need_geo_condn == False

        # self._data_index = self._create_data_index()

        showers_list = []
        energy_list = []
        phi_list = []
        theta_list = []
        if self.need_geo_condn:
            geo_list = []
        for f in self.files:
            if self.need_geo_condn:
                geo_str, fi = f[0], f[1]
                showers, energy, phi, theta = load_showers(fi)
            else:
                showers, energy, phi, theta = load_showers(f)
            showers_list.append(showers)
            energy_list.append(energy)
            phi_list.append(phi)
            theta_list.append(theta)
            if self.need_geo_condn:
                geo_list.append(preprocess_geo(energy.shape[0], geo_str, train_on))

        self.showers = np.concatenate(showers_list)
        self.energy = np.concatenate(energy_list)
        self.phi = np.concatenate(phi_list)
        self.theta = np.concatenate(theta_list)
        if self.need_geo_condn:
            self.geo = np.concatenate(geo_list)

        file_list = "\n".join(f"- {file}" for file in self.files)
        logger.info(f"Loaded {len(self.showers)} showers from the following files:\n{file_list}")
        
        if max_num_showers is not None:
            logger.info(f"Limiting the number of showers to {max_num_showers}.")
            # self._data_index = self._data_index[:max_num_showers]
            self.showers = self.showers[:max_num_showers]
            self.energy = self.energy[:max_num_showers]
            self.phi = self.phi[:max_num_showers]
            self.theta = self.theta[:max_num_showers]
            if self.need_geo_condn:
                self.geo = self.geo[:max_num_showers]

        self.use_cond_info = use_cond_info
        self.max_num_showers = max_num_showers

    def _create_data_index(self):
        """Creates an index for accessing the data spread across multiple .h5 files."""
        data_index = []
        for file_idx, file_path in enumerate(self.files):
            with h5py.File(file_path, "r") as f:
                data_length = len(f["showers"])
            data_index.extend([(file_idx, i) for i in range(data_length)])
        return data_index

    def _torch(self, *xs):
        if len(xs) == 1:
            return torch.from_numpy(xs[0]).float()
        else:
            return tuple(self._torch(*x) if isinstance(x, tuple) else self._torch(x) for x in xs)

    def __len__(self):
        # return len(self._data_index)
        return len(self.showers)

    def __getitem__(self, idx):
        # file_idx, data_idx = self._data_index[idx]
        # if self.use_cond_info:
        #     showers, energy, phi, theta = load_showers(self.files[file_idx])
        #     return self._torch(
        #         showers[data_idx][None, ...], # add channel dimension
        #         (
        #             energy[data_idx:data_idx+1],
        #             phi[data_idx:data_idx+1],
        #             theta[data_idx:data_idx+1],
        #         )
        #     )

        # else:
        #     showers = load_showers(self.files[file_idx], only_showers=True)
        #     return self._torch(showers[data_idx][None, ...])
        
        if self.use_cond_info:
            return self._torch(
                self.showers[idx][None, ...], # add channel dimension
                (
                    self.energy[idx:idx+1],
                    self.phi[idx:idx+1],
                    self.theta[idx:idx+1],
                ) if not self.need_geo_condn else
                (
                    self.energy[idx:idx+1],
                    self.phi[idx:idx+1],
                    self.theta[idx:idx+1],
                    self.geo[idx:idx+1],
                )
            )

        else:
            return self._torch(self.showers[idx][None, ...])
