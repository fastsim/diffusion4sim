from pathlib import Path
from typing import Union

import h5py
import numpy as np


def load_showers(file_path: Union[str, Path], only_showers: bool = False, convert_MeV_to_GeV: bool = True):
    scale = 1000.0 if convert_MeV_to_GeV else 1.0
    with h5py.File(file_path, "r") as f:
        showers = f["showers"][:].astype(np.float32) / scale
        energy = f["incident_energy"][:].astype(np.float32) / scale
        phi = f["incident_phi"][:].astype(np.float32)
        theta = f["incident_theta"][:].astype(np.float32)

    if only_showers:
        return showers
    else:
        return showers, energy, phi, theta


def save_showers(showers: np.ndarray, energy: Union[int, float, np.ndarray], phi: Union[float, np.ndarray], theta: Union[float, np.ndarray], output_path: Union[str, Path], convert_GeV_to_MeV: bool = True):
    scale = 1000.0 if convert_GeV_to_MeV else 1.0
    showers = showers * scale
    energy = energy * scale
    
    # Check if the input is a single value and broadcast it to the length of showers
    if isinstance(energy, (int, float)):
        energy = np.full(len(showers), energy)
    if isinstance(theta, float):
        theta = np.full(len(showers), theta)
    if isinstance(phi, float):
        phi = np.full(len(showers), phi)

    with h5py.File(output_path, "w") as f:
        f.create_dataset("showers", data=showers, compression="gzip", compression_opts=9)
        f.create_dataset("incident_energy", data=energy, compression="gzip", compression_opts=9)
        f.create_dataset("incident_phi", data=phi, compression="gzip", compression_opts=9)
        f.create_dataset("incident_theta", data=theta, compression="gzip", compression_opts=9)