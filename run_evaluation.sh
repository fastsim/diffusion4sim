#!/bin/bash
# set -e
export CUDA_VISIBLE_DEVICES=0

# print environment
nvidia-smi
hostname;pwd;date


declare -A model_path=(
    ["heun_32_large"]="./chkpt/experiments/calodit_edm/edm_multi_resume/2024-10-19_09-17-03/final_model.pt"
    ["cd_1_to_convert"]="./chkpt/experiments/calodit_consistency_distillation/cd_1_large_resume/2024-11-01_03-02-57/final_model.pt"
)

declare -A diffusion_class=(
    ["heun_32_large"]="src.diffusion.EDM"
    ["cd_1_to_convert"]="src.diffusion.ConsistencyModel"
)


declare -A sampling_args=(
    ["heun_32_large"]="sampling.solver=heun sampling.steps=32 "
    ["cd_1_to_convert"]="sampling.steps=1 "
)
echo "Running evaluation..."

for model in "${!model_path[@]}"; do
    output_dir="simulations/${model}"

    if [ -d $output_dir ]; then
        echo "Skipping..."
    else
        python scripts/validate.py \
            model.diffusion_class=${diffusion_class[$model]} \
            model.model_path=${model_path[$model]} \
            validate.output_dir=${output_dir} \
            ${sampling_args[$model]}
    fi
done
echo "Done."