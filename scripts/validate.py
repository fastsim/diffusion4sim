"""Generate showers under the same conditions and compare with the full simulation."""
import sys
from pathlib import Path

import numpy as np
import rootutils
from dotenv import load_dotenv
from omegaconf import DictConfig, OmegaConf
from torch.utils.data import DataLoader
from tqdm import tqdm

rootutils.setup_root(__file__, pythonpath=True)

from src.data.dataset import CaloShowerDataset
from src.data.preprocessing import CaloShowerPreprocessor, cut_below_noise_level
from src.data.utils import save_showers
from src.diffusion.base import create_model_from_config
from src.evaluation.utils import compare_observables
from src.utils import get_conditions_str, get_logger, load_config, setup_accelerator


@load_config(default_config_path="configs/validate/default.yaml")
def main(cfg: DictConfig):
    # setup accelerator and logger
    accelerator = setup_accelerator(**cfg.accelerator)

    logger = get_logger()

    logger.info(f"Config:\n{OmegaConf.to_yaml(cfg)}")

    # load model
    model = create_model_from_config(cfg.model)
    model = accelerator.prepare(model)
    # model.summarize()
  
    preprocessor = CaloShowerPreprocessor(**model.config.preprocessing)

    batch_size = int(cfg.validate.batch_size)
    for geometry, energy, phi, theta, fullsim_path in cfg.validate.simulation_conditions:
        conditions_str = get_conditions_str(geometry, energy, phi, theta)
        output_dir = Path(cfg.validate.output_dir) / conditions_str
        output_dir.mkdir(parents=True, exist_ok=True)

        if cfg.validate.need_geo_condn:
            file_struc = [[geometry, fullsim_path],]
        else:
            file_struc = [fullsim_path,]
        dataset = CaloShowerDataset(files=file_struc, need_geo_condn=cfg.validate.need_geo_condn, train_on=cfg.validate.train_on)
        dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False)
        dataloader = accelerator.prepare(dataloader)

        num_samples = len(dataset)

        logger.info(
            f"Generating {num_samples} events for geometry {geometry}, energy {energy} GeV, phi {phi} and theta {theta}"
        )

        generated_events_list = []
        orginal_events_list = []
        for sample in tqdm(dataloader):
            showers, conditions = sample
            _, conditions = preprocessor.transform(conditions=conditions)
            
            showers = cut_below_noise_level(showers, noise_level=preprocessor.shower_preprocessor.noise_level)
            orginal_events_list.append(showers.squeeze(1).cpu().numpy())

            generated_events = model.sample(conditions=conditions, progress=True, **cfg.sampling).squeeze(1)  # squeeze to remove the output channel dimension
            generated_events, _ = preprocessor.inverse_transform(generated_events, conditions)
            generated_events_list.append(generated_events.cpu().numpy())

        original_events = np.concatenate(orginal_events_list)
        generated_events = np.concatenate(generated_events_list)

        output_path = (
            output_dir / f"generated_{num_samples}events_Geo_{geometry}_E_{energy}GeV_Phi_{phi}_Theta_{theta}.h5"
        )

        if output_path.exists():
            logger.warning(f"File {output_path} already exists, overwriting")
            output_path.unlink()

        save_showers(generated_events, energy, phi, theta, output_path)
        logger.info(f"Saved generated events to {output_path}")

        compare_observables(original_events, generated_events, output_dir, geometry, energy, phi, theta)

    logger.info("Validation completed.")


if __name__ == "__main__":
    load_dotenv()
    sys.exit(main())
