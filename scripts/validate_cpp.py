"""Generate showers under the same conditions and compare with the full simulation."""
import sys
from pathlib import Path

import numpy as np
import torch
import rootutils
from dotenv import load_dotenv
from omegaconf import DictConfig, OmegaConf
from torch.utils.data import DataLoader
from tqdm import tqdm
import onnxruntime as ort

rootutils.setup_root(__file__, pythonpath=True)

from src.data.dataset import CaloShowerDataset
from src.data.preprocessing import cut_below_noise_level
from src.data.utils import save_showers
from src.evaluation.utils import compare_observables
from src.utils import get_conditions_str, load_config


def load_model(model_path, model_type):
    if model_type=='ts_cpu':
        model = torch.jit.load(model_path, map_location=torch.device('cpu'))
    elif model_type=='ts_gpu':
        model = torch.jit.load(model_path, map_location=torch.device('cuda'))
    elif model_type=='onnx_cpu':
        model = ort.InferenceSession(model_path, providers=['CPUExecutionProvider'])
    elif model_type=='onnx_gpu':
        model = ort.InferenceSession(model_path, providers=['CUDAExecutionProvider'])
    return model

def sampling_fn(model, model_type, conditions):
    conditions = torch.cat(conditions, dim=-1)
    if model_type in ['onnx_cpu', 'onnx_gpu']:
        return model.run(
            ["output"], {
                "conditions": conditions.numpy(),
            })[0]
    else:
        result = model(conditions)
        import pdb;pdb.set_trace()
        return model(conditions).detach().cpu().numpy()


@load_config(default_config_path="configs/validate/cpp.yaml")
def main(cfg: DictConfig):
    # load model
    model_type = cfg.validate.model_type
    model = load_model(cfg.validate.model_path, model_type)

    batch_size = int(cfg.validate.batch_size)
    for geometry, energy, phi, theta, fullsim_path in cfg.validate.simulation_conditions:
        conditions_str = get_conditions_str(geometry, energy, phi, theta)
        output_dir = Path(cfg.validate.output_dir) / conditions_str
        output_dir.mkdir(parents=True, exist_ok=True)

        if cfg.validate.need_geo_condn:
            file_struc = [[geometry, fullsim_path],]
        else:
            file_struc = [fullsim_path,]
        dataset = CaloShowerDataset(files=file_struc, need_geo_condn=cfg.validate.need_geo_condn, train_on=cfg.validate.train_on)
        dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False)

        num_samples = len(dataset)

        generated_events_list = []
        orginal_events_list = []
        for sample in tqdm(dataloader):
            showers, conditions = sample
            if model_type=='ts_gpu':
                conditions = [c.cuda() for c in conditions]
            conditions[-1] = conditions[-1].view(-1, 5)
            
            showers = cut_below_noise_level(showers, noise_level=cfg.shower_preprocessor.noise_level)
            orginal_events_list.append(showers.squeeze(1).cpu().numpy())

            generated_events = sampling_fn(model, model_type, conditions) #.squeeze(1)  # squeeze to remove the output channel dimension
            generated_events_list.append(generated_events)

        original_events = np.concatenate(orginal_events_list)
        generated_events = np.concatenate(generated_events_list)

        output_path = (
            output_dir / f"generated_{num_samples}events_Geo_{geometry}_E_{energy}GeV_Phi_{phi}_Theta_{theta}.h5"
        )

        if output_path.exists():
            output_path.unlink()

        save_showers(generated_events, energy, phi, theta, output_path)
        compare_observables(original_events, generated_events, output_dir, geometry, energy, phi, theta)



if __name__ == "__main__":
    load_dotenv()
    sys.exit(main())
