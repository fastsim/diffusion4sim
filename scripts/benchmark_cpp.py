import math
import os
import sys
import time
from pathlib import Path

import numpy as np
import pandas as pd
import rootutils
import torch
from dotenv import load_dotenv
from omegaconf import DictConfig, OmegaConf
from tqdm import tqdm
import onnxruntime as ort

rootutils.setup_root(__file__, pythonpath=True)

from src.utils import load_config


def load_model(model_path, model_type):
    if model_type=='ts_cpu':
        model = torch.jit.load(model_path, map_location=torch.device('cpu'))
    elif model_type=='ts_gpu':
        model = torch.jit.load(model_path, map_location=torch.device('cuda'))
    elif model_type=='onnx_cpu':
        sess_options = ort.SessionOptions()
        sess_options.intra_op_num_threads = 1
        sess_options.inter_op_num_threads = 1
        model = ort.InferenceSession(model_path, sess_options, providers=['CPUExecutionProvider'])
    elif model_type=='onnx_gpu':
        model = ort.InferenceSession(model_path, providers=['CUDAExecutionProvider'])
    return model

def sampling_fn(model, model_type, conditions):
    with torch.no_grad():
        if model_type in ['onnx_cpu', 'onnx_gpu']:
            return model.run(
                ["output"], {
                    "energy": conditions[0].numpy(),
                    "phi": conditions[1].numpy(),
                    "theta": conditions[2].numpy(),
                    "geo": conditions[3].numpy(),
                })[0]
        else:
            return model(*conditions)

@load_config(default_config_path="configs/benchmark/cpp.yaml")
def main(cfg: DictConfig):
    # load model
    model_type = cfg.benchmark.model_type
    if model_type=="ts_cpu":
        torch.set_num_interop_threads(1)
        torch.set_num_threads(1)

    model = load_model(cfg.benchmark.model_path, model_type)

    num_rounds = int(cfg.benchmark.num_rounds)
    num_samples = int(cfg.benchmark.num_samples)
    batch_size = int(cfg.benchmark.batch_size)

    cond_e = torch.FloatTensor(batch_size, 1).uniform_(1, 1000)
    cond_phi = torch.FloatTensor(batch_size, 1).uniform_(-torch.pi, torch.pi)
    cond_theta = torch.FloatTensor(batch_size, 1).uniform_(0.87, 2.27)
    if cfg.benchmark.need_geo_condn:
        cond_geo = torch.FloatTensor(batch_size * [1, 0, 0, 0, 0]).view(batch_size, -1)

    conditions = [cond_e, cond_phi, cond_theta]
    if cfg.benchmark.need_geo_condn:
        conditions += [cond_geo,]
    
    if model_type=='ts_gpu':
        conditions = [c.cuda() for c in conditions]

    times_per_shower = []
    for i in tqdm(range(num_rounds)):
        total_time_ms = 0.0
        num_iterations = math.ceil(num_samples / batch_size)

        if cfg.benchmark.skip_first_round and i==0:
            for _ in range(num_iterations):
                sampling_fn(model, model_type, conditions)

        for _ in tqdm(range(num_iterations)):
            start = time.perf_counter_ns()

            sampling_fn(model, model_type, conditions)

            stop = time.perf_counter_ns()
         
            total_time_ms += (stop - start) * 1e-6
        
        time_per_shower = total_time_ms / (batch_size * num_iterations)
        times_per_shower.append(time_per_shower)

    df = pd.DataFrame({
        "model": [cfg.benchmark.name],
        "batch_size": [batch_size],
        "mean_time_per_shower": [np.mean(times_per_shower)],
        "std_time_per_shower": [np.std(times_per_shower)],
    })

    output_path = Path(cfg.benchmark.output_path)
    if output_path.exists():
        df.to_csv(output_path, mode="a", header=False, index=False)
    else:
        df.to_csv(output_path, index=False)

if __name__ == "__main__":
    load_dotenv()
    sys.exit(main())
