import math
import os
import sys
import time
from pathlib import Path

import numpy as np
import pandas as pd
import rootutils
import torch
from dotenv import load_dotenv
from omegaconf import DictConfig, OmegaConf
from tqdm import tqdm

rootutils.setup_root(__file__, pythonpath=True)

from src.data.preprocessing import CaloShowerPreprocessor
from src.diffusion.base import create_model_from_config
from src.utils import get_logger, load_config, setup_accelerator, to_device


@load_config(default_config_path="configs/benchmark/default.yaml")
def main(cfg: DictConfig):
    # setup accelerator and logger
    accelerator = setup_accelerator(**cfg.accelerator)

    logger = get_logger()

    logger.info(f"Config:\n{OmegaConf.to_yaml(cfg)}")

    # load model
    model = create_model_from_config(cfg.model)
    model = accelerator.prepare(model)
    logger.info(f"Model:\n{model}")

    preprocessor = CaloShowerPreprocessor(**model.config.preprocessing)

    num_rounds = int(cfg.benchmark.num_rounds)
    num_samples = int(cfg.benchmark.num_samples)
    batch_size = int(cfg.benchmark.batch_size)

    cond_e = torch.FloatTensor(batch_size, 1).uniform_(1, 1000)
    cond_phi = torch.FloatTensor(batch_size, 1).uniform_(-torch.pi, torch.pi)
    cond_theta = torch.FloatTensor(batch_size, 1).uniform_(0.87, 2.27)
    _, conditions = preprocessor.transform(conditions=(cond_e, cond_phi, cond_theta))
    conditions = to_device(conditions, accelerator.device)

    def sampling_fn():
        return model.sample(conditions=conditions, progress=False, **cfg.sampling)

    times_per_shower = []
    for i in tqdm(range(num_rounds)):
        total_time_ms = 0.0
        
        if cfg.benchmark.skip_first:
            sampling_fn()

        num_iterations = math.ceil(num_samples / batch_size)
        for _ in tqdm(range(num_iterations)):
            
            if accelerator.device.type == "cuda":
                torch.cuda.synchronize()

            start = time.perf_counter_ns()

            sampling_fn()
            if accelerator.device.type == "cuda":
                torch.cuda.synchronize()

            stop = time.perf_counter_ns()
         
            total_time_ms += (stop - start) * 1e-6
        
        time_per_shower = total_time_ms / (batch_size * num_iterations)
        times_per_shower.append(time_per_shower)
        logger.info(f"(Round {i}) Total time: {total_time_ms} [ms]")
        logger.info(f"(Round {i}) Time / Shower: {time_per_shower} [ms]")

    logger.info(f"*** Model: {cfg.benchmark.name}")
    logger.info(f"*** Device: {accelerator.device} ***")
    logger.info(f"*** Batch size: {batch_size} ***")
    logger.info(f"*** NFE: {cfg.sampling.steps} ***")
    logger.info(f"*** Time / Shower: {np.mean(times_per_shower):.6f} ± {np.std(times_per_shower):.6f} [ms] ***")

    df = pd.DataFrame({
        "model": [cfg.benchmark.name],
        "device": [accelerator.device],
        "batch_size": [batch_size],
        "nfe": [cfg.sampling.steps],
        "mean_time_per_shower": [np.mean(times_per_shower)],
        "std_time_per_shower": [np.std(times_per_shower)],
    })

    output_path = Path(cfg.benchmark.output_path)
    if output_path.exists():
        df.to_csv(output_path, mode="a", header=False, index=False)
    else:
        df.to_csv(output_path, index=False)

if __name__ == "__main__":
    load_dotenv()
    sys.exit(main())
