"""Convert model to TorchScript (GPU/CPU separate) and ONNX. One model for all batch sizes."""
import sys
from time import time
from pathlib import Path

import numpy as np
import torch
import rootutils
from dotenv import load_dotenv
from omegaconf import DictConfig, OmegaConf
from torch.utils.data import DataLoader
from tqdm import tqdm

rootutils.setup_root(__file__, pythonpath=True)

from src.data.preprocessing import CaloShowerPreprocessor
from src.diffusion.base import create_model_from_config
from src.utils import load_config


class Wrapped(torch.nn.Module):
    def __init__(self, model, preprocessor, steps):
        super(Wrapped, self).__init__()
        self.model = model
        self.preprocessor = preprocessor
        self.steps = steps

    def forward(self, conditions):
        if conditions.shape[1]==3:
            conditions = conditions.reshape(1, -1)
            energy, phi, theta = conditions[:, 0], conditions[:, 1], conditions[:, 2]
            geo = None
        else:
            conditions = conditions.reshape(1, -1)
            energy, phi, theta = conditions[:, 0], conditions[:, 1], conditions[:, 2]
            geo = conditions[:, 3:]
        _, conditions = self.preprocessor.transform(conditions=[energy, phi, theta] + [geo,] if geo is not None else [])
        generated_events = self.model.sample(conditions=conditions, steps=self.steps).squeeze(1)
        generated_events, _ = self.preprocessor.inverse_transform(generated_events, conditions)
        return generated_events


@load_config(default_config_path="configs/convert/default.yaml")
def main(cfg: DictConfig):
    output_dir = Path(cfg.convert.output_dir)
    output_dir.mkdir(parents=True, exist_ok=True)

    # load model
    model = create_model_from_config(cfg.model)
    preprocessor = CaloShowerPreprocessor(**model.config.preprocessing)

    wrapped_model = Wrapped(model, preprocessor, cfg.sampling.steps)
    wrapped_model.eval()

    # [energy, phi, theta] + [geo](optional)
    dummy_inputs = torch.randn(1, 3)  # 3 + 5
    if model.config.data.train.need_geo_condn:
        dummy_inputs = torch.cat([dummy_inputs, torch.randn(1, len(model.config.data.train.train_on) + 1)], -1)

    # TorchScript CPU
    output_path = (output_dir / f"cd_cpu.pt")
    with torch.no_grad():
        traced = torch.jit.trace(wrapped_model, dummy_inputs)
        traced.save(output_path)
        traced(dummy_inputs)
    print("CPU Done.")


    # TorchScript GPU
    output_path = (output_dir / f"cd_gpu.pt")
    wrapped_model.cuda()
    dummy_inputs = dummy_inputs.cuda()
    with torch.no_grad():
        traced = torch.jit.trace(wrapped_model, dummy_inputs)
        traced.save(output_path)
        traced(dummy_inputs)
    print("GPU Done.")


    # ONNX
    wrapped_model.cpu()
    dummy_inputs = dummy_inputs.cpu()
    output_path = (output_dir / f"cd.onnx")

    output_names = ["output"]
    input_names = ["conditions"]
    
    dynamic_axes={'conditions' : {0 : 'batch_size'},}

    with torch.no_grad():
        torch.onnx.export(wrapped_model, dummy_inputs, output_path,
            input_names=input_names, output_names=output_names, dynamic_axes=dynamic_axes, opset_version=16)
    print("ONNX Done.")


if __name__ == "__main__":
    load_dotenv()
    sys.exit(main())
